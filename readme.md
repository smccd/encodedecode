# EncodeDecode

**EncodeDecode** is a library for encoding and decoding data in PHP arrays.

### Usage Instructions

#### Installation

If you are using [composer](http://getcomposer.org/), just integrate the following code into your `composer.json` file and run composer.

    {
    "repositories": [
        {
            "type": "git",
            "url": "https://smccd.bitbucket.org/encodedecode.git"
        }
    ],
    "require": {
        "smccd/encodedecode": "dev-master"
    }
}

If you are not using composer you can download a copy of the EncodeDecode files manually and include the top "EncodeDecode.php" class in your project.

#### Creating EncodeDecode object
To use EncodeDecode, simply invoke one of the static methods, passing in an array.
    
    use Smccd;

    $data = ['value1', 'value2'];

    EncodeDecode\EncodeDecode::encode($data); 

or

    $data = ['value1', 'value2'];
    
    EncodeDecode\EncodeDecode::decode($data); 


