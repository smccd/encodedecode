<?php

/**
 * EncodeDecode
 *
 * Library to HMTL Encode and Decode array strings.
 *
 * @version 0.1
 * @licence MIT License
 * @source: http://bitbucket.org/smccd/encodedecode.git
 *
 */

// PSR-0 Autoloader
// see: http://zaemis.blogspot.fr/2012/05/writing-minimal-psr-0-autoloader.html
spl_autoload_register(function ($classname) {
	$classname = ltrim($classname, "\\");
	preg_match('/^(.+)?([^\\\\]+)$/U', $classname, $match);
	$classname = 'src/'.str_replace("\\", "/", $match[1])
		. str_replace(array("\\", "_"), "/", $match[2])
		. ".php";
	include_once $classname;
});
