<?php

namespace Smccd;

class Encode {
    
    public static function encode($data) {
        if (is_array($data)) {
            return array_map(array(new static(), 'encode'), $data);
        }
        if (is_object($data)) {
            $tmp = clone $data; // avoid modifing original object
            foreach ( $data as $k => $var )
                $tmp->{$k} = static::encode($var);
            return $tmp;
        }
        return htmlentities($data);
    }

   
}