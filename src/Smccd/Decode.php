<?php

namespace Smccd;

class Decode {
    
    public static function decode($data) {
        if (is_array($data)) {
            return array_map(array(new static,'decode'), $data);
        }
        if (is_object($data)) {
            $tmp = clone $data; // avoid modifing original object
            foreach ( $data as $k => $var )
                $tmp->{$k} = static::decode($var);
            return $tmp;
        }

        $data = html_entity_decode($data);
        return $data;
    }

   
}